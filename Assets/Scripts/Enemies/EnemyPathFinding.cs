using Misc;
using UnityEngine;

namespace Enemies
{
    public class EnemyPathFinding : MonoBehaviour
    {
        [SerializeField] private float moveSpeed = 2f;
        private Rigidbody2D _rigidbody2D;
        private Vector2 _roamDirection;
        private KnockBack _knockBack;
        private SpriteRenderer _spriteRenderer;

        private void Awake()
        {
            _knockBack = GetComponent<KnockBack>();
            _rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void SetRoamDirection(Vector2 newDirection)
        {
            _roamDirection = newDirection;
        }


        private void FixedUpdate()
        {
            if (!_knockBack.GettingKnockedBack)
            {
                Move();
            }

            UpdateSpriteDirection();
        }

        private void UpdateSpriteDirection()
        {
            if (_roamDirection.x < 0)
            {
                _spriteRenderer.flipX = true;
            }
            else if(_roamDirection.x > 0)
            {
                _spriteRenderer.flipX = false;
            }
        }

        private void Move()
        {
            _rigidbody2D.MovePosition(_rigidbody2D.position + _roamDirection * (moveSpeed * Time.fixedDeltaTime));
        }

        public void StopMoving()
        {
            _roamDirection = Vector2.zero;
        }
    }
}