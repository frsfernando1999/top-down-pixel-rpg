using Player;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemies
{
    [RequireComponent(typeof(EnemyPathFinding))]
    public class EnemyAI : MonoBehaviour
    {
        [SerializeField] private float changeDirectionTime = 2f;
        [SerializeField] private float attackRange;
        [SerializeField] private MonoBehaviour enemyType;
        [SerializeField] private bool stopMovingWhileAttacking;
        
        private Vector2 _roamPosition;
        private float _timeRoaming;

        private enum EnemyState
        {
            Roaming,
            Attacking
        }

        private EnemyState _enemyState;
        private EnemyPathFinding _pathFinding;

        private void Awake()
        {
            _pathFinding = GetComponent<EnemyPathFinding>();
            _enemyState = EnemyState.Roaming;
        }


        private void Start()
        {
            _roamPosition = GetRoamingPosition();
        }

        private void Update()
        {
            MovementStateControl();
        }

        private void MovementStateControl()
        {
            switch (_enemyState)
            {
                case EnemyState.Roaming:
                    Roaming();
                    break;
                case EnemyState.Attacking:
                    Attacking();
                    break;
            }
        }

        private void Roaming()
        {
            _timeRoaming += Time.deltaTime;
            _pathFinding.SetRoamDirection(_roamPosition);

            if (Vector2.Distance(PlayerController.Instance.transform.position, transform.position) < attackRange)
            {
                _enemyState = EnemyState.Attacking;
            }

            if (_timeRoaming > changeDirectionTime)
            {
                _roamPosition = GetRoamingPosition();
            }
        }

        private void Attacking()
        {
            if (Vector2.Distance(PlayerController.Instance.transform.position, transform.position) > attackRange)
            {
                _enemyState = EnemyState.Roaming;
            }
            
            if (attackRange == 0) return;
            
            if (enemyType is not IEnemy enemy) return;
            
            enemy.Attack();

            if (stopMovingWhileAttacking)
            {
                _pathFinding.StopMoving();
            }
            else
            {
                _pathFinding.SetRoamDirection(_roamPosition);
            }
        }
        
        private Vector2 GetRoamingPosition()
        {
            _timeRoaming = 0f;
            return new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;
        }
    }
}