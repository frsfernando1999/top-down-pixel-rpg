namespace Enemies
{
    public interface IEnemy
    {
        public void Attack();
    }
}
