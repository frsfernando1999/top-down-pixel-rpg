using System.Collections;
using Player;
using UnityEngine;
using Weapons;

namespace Enemies
{
    public class Shooter : MonoBehaviour, IEnemy
    {
        [SerializeField] private Projectile bulletPrefab;
        [SerializeField] [Min(1)] private float bulletMoveSpeed = 6;
        [SerializeField] [Min(1)] private int burstCount = 5;
        [SerializeField] [Min(1)] private int projectilePerBurst = 8;
        [SerializeField] [Range(0, 359)] private float angleSpread = 120;
        [SerializeField] [Min(0.1f)] private float startingDistance = 0.1f;
        [Tooltip("Time between bullet bursts")]
        [SerializeField] [Min(0.1f)] private float timeBetweenBursts = 0.75f;
        [Tooltip("Time between attacks")]
        [SerializeField] [Min(0.1f)] private float restTime = 1f;
        [Tooltip("Makes bullets go back and forth on the angle spread")]
        [SerializeField] private bool oscillate;

        private bool _stagger;
        private bool _isShooting;

        private void OnValidate()
        {
            if (oscillate) _stagger = true;
            if (!oscillate) _stagger = false;
            if (angleSpread == 0) projectilePerBurst = 1;
            
        }

        public void Attack()
        {
            if (!_isShooting)
            {
                StartCoroutine(ShootRoutine());
            }
        }

        private IEnumerator ShootRoutine()
        {
            _isShooting = true;
            float timeBetweenProjectiles = 0f;

            TargetConeOfInfluence(out var startAngle, out var currentAngle, out var angleStep, out var endAngle);

            if (_stagger) timeBetweenProjectiles = timeBetweenBursts / projectilePerBurst;

            for (int i = 0; i < burstCount; i++)
            {
                if (!oscillate)
                {
                    TargetConeOfInfluence(out startAngle, out currentAngle, out angleStep, out endAngle);
                }

                if (oscillate && i % 2 != 1)
                {
                    TargetConeOfInfluence(out startAngle, out currentAngle, out angleStep, out endAngle);
                }
                else if (oscillate)
                {
                    currentAngle = endAngle;
                    endAngle = startAngle;
                    startAngle = currentAngle;
                    angleStep *= -1;
                }


                for (int j = 0; j < projectilePerBurst; j++)
                {
                    Vector2 pos = FindBulletSpawnPos(currentAngle);
                    Projectile newBullet = Instantiate(bulletPrefab, pos, Quaternion.identity);
                    newBullet.transform.right = newBullet.transform.position - transform.position;
                    newBullet.UpdateMoveSpeed(bulletMoveSpeed);
                    currentAngle += angleStep;
                    if (_stagger)
                    {
                        yield return new WaitForSeconds(timeBetweenProjectiles);
                    }
                }

                if (!_stagger)
                {
                    yield return new WaitForSeconds(timeBetweenBursts);
                }
            }

            yield return new WaitForSeconds(restTime);
            _isShooting = false;
        }

        private void TargetConeOfInfluence(out float startAngle, out float currentAngle, out float angleStep,
            out float endAngle)
        {
            Vector2 targetDirection = PlayerController.Instance.transform.position - transform.position;
            float targetAngle = Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg;
            startAngle = targetAngle;
            currentAngle = targetAngle;
            endAngle = targetAngle;
            angleStep = 0;

            if (angleSpread != 0)
            {
                angleStep = angleSpread / (projectilePerBurst - 1);
                var halfAngleSpread = angleSpread / 2f;
                startAngle = targetAngle - halfAngleSpread;
                endAngle = targetAngle + halfAngleSpread;
                currentAngle = startAngle;
            }
        }

        private Vector2 FindBulletSpawnPos(float currentAngle)
        {
            var position = transform.position;
            float x = position.x + startingDistance * Mathf.Cos(currentAngle * Mathf.Deg2Rad);
            float y = position.y + startingDistance * Mathf.Sin(currentAngle * Mathf.Deg2Rad);

            Vector2 pos = new Vector2(x, y);

            return pos;
        }
    }
}