using System.Collections;
using Misc;
using Player;
using Unity.Mathematics;
using UnityEngine;

namespace Enemies
{
    public class EnemyHealth : MonoBehaviour
    {
        [SerializeField] private int maxHealth = 3;
        [SerializeField] private float deathTimer = 0.15f;
        [SerializeField] private float knockBackThrustForce = 8f;
        [SerializeField] private ParticleSystem deathVFX;
        
        private PickupSpawner _pickupSpawner;
        
        private int _currentHealth;
        private KnockBack _knockBack;
        private Flash _flash;

        private void Awake()
        {
            _knockBack = GetComponent<KnockBack>();
            _flash = GetComponent<Flash>();
            _pickupSpawner = GetComponent<PickupSpawner>();
        }

        private void Start()
        {
            _currentHealth = maxHealth;
        }
    
        public void TakeDamage(int damage)
        {
            _currentHealth -= damage;
            _knockBack.GetKnockedBack(PlayerController.Instance.transform, knockBackThrustForce);
            StartCoroutine(_flash.FlashRoutine());
            if (_currentHealth <= 0)
            {
                StartCoroutine(Die());
            }
        }

        private IEnumerator Die()
        {
            yield return new WaitForSeconds(deathTimer);
            Instantiate(deathVFX, transform.position, quaternion.identity);
            _pickupSpawner.DropItems();
            Destroy(gameObject);
        }
    }
}
