using System;
using System.Collections;
using Player;
using UnityEngine;
using Weapons;

namespace Enemies
{
    public class GrapeSlime : MonoBehaviour, IEnemy
    {
        [SerializeField] private GrapeProjectile grapeProjectilePrefab;
        [SerializeField] private float attackCooldown = 1f;

        private bool _canAttack = true;

        private Animator _animator;
        private SpriteRenderer _spriteRenderer;
        private static readonly int Fire = Animator.StringToHash("Fire");

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void Attack()
        {
            if (_canAttack)
            {
                _canAttack = false;
                _animator.SetTrigger(Fire);
                _spriteRenderer.flipX = transform.position.x - PlayerController.Instance.transform.position.x > 0;
                StartCoroutine(ResetAttackCooldown());
            }
        }

        private IEnumerator ResetAttackCooldown()
        {
            yield return new WaitForSeconds(attackCooldown);
            _canAttack = true;
        }

        public void SpawnGrapeAnimEvent()
        {
            Instantiate(grapeProjectilePrefab, transform.position, Quaternion.identity);
        }
    }
}