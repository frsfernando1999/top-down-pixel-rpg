using System.Collections;
using Player;
using Unity.Mathematics;
using UnityEngine;

namespace Enemies
{
    public class GrapeProjectile : MonoBehaviour
    {
        [SerializeField] private float flightDuration = 1f;
        [SerializeField] private float heightY = 3f;
        [SerializeField] private AnimationCurve animationCurve;
        [SerializeField] private GameObject shadowPrefab;
        [SerializeField] private GrapeLandSplatter splatterPrefab;

        private void Start()
        {
            GameObject grapeShadow = Instantiate(shadowPrefab, transform.position + new Vector3(0f, -0.3f, 0f), Quaternion.identity);
            
            Vector3 playerPos = PlayerController.Instance.transform.position;
            Vector3 grapeShadowStartPos = grapeShadow.transform.position;
            
            StartCoroutine(MoveShadowRoutine(grapeShadow, grapeShadowStartPos, playerPos));
            StartCoroutine(ProjectileCurveRoutine(transform.position, playerPos));
        }

        private IEnumerator ProjectileCurveRoutine(Vector3 startPos, Vector3 endPos)
        {
            float timePassed = 0;

            while (timePassed < flightDuration)
            {
                timePassed += Time.deltaTime;
                float linearT = timePassed / flightDuration;
                float heightT = animationCurve.Evaluate(linearT);
                float height = Mathf.Lerp(0f, heightY, heightT);
                transform.position = Vector2.Lerp(startPos, endPos, linearT) + new Vector2(0f, height);
                yield return null;
            }

            Instantiate(splatterPrefab, transform.position, quaternion.identity);
            Destroy(gameObject);
        }

        private IEnumerator MoveShadowRoutine(GameObject grapeShadow, Vector3 startPos, Vector3 endPos)
        {
            float timePassed = 0;

            while (timePassed < flightDuration)
            {
                timePassed += Time.deltaTime;
                float linearT = timePassed / flightDuration;
                grapeShadow.transform.position = Vector2.Lerp(startPos, endPos, linearT);
                yield return null;
            }
            Destroy(grapeShadow.gameObject);    
        }
    }
}