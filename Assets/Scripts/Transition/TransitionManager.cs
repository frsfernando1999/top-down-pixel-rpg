using Misc;

namespace Transition
{
    public class TransitionManager : Singleton<TransitionManager>
    {
        public PortalIdentifier SceneIdentifier { get; private set; }

        public void SetTransitionName(PortalIdentifier sceneIdentifier)
        {
            SceneIdentifier = sceneIdentifier;
        }
    }
}
