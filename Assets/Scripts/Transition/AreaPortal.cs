using System.Collections;
using Player;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Transition
{
    public class AreaPortal : MonoBehaviour
    {
        [SerializeField] private string sceneToLoad;
        [SerializeField] private Transform spawnPoint;
        [SerializeField] private PortalIdentifier portalIdentifier;

        private void Start()
        {
            if (TransitionManager.Instance.SceneIdentifier == portalIdentifier)
            {
                PlayerController.Instance.transform.position = spawnPoint.position;
            }

            CameraController.Instance.SetPlayerCameraFollow();
            UIFade.Instance.FadeClear();
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (sceneToLoad == "none") return;
            if (!col.CompareTag("Player")) return;
            TransitionManager.Instance.SetTransitionName(portalIdentifier);
            UIFade.Instance.FadeToBlack();
            StartCoroutine(LoadSceneRoutine());
        }

        private IEnumerator LoadSceneRoutine()
        {
            yield return new WaitForSeconds(UIFade.Instance.FadeTime);
            SceneManager.LoadScene(sceneToLoad);
        }
    }
}