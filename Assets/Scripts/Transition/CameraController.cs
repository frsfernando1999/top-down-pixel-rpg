using Cinemachine;
using Misc;
using Player;

namespace Transition
{
    public class CameraController : Singleton<CameraController>
    {
        private CinemachineVirtualCamera _cinemachineVirtualCamera;
        
        public void SetPlayerCameraFollow()
        {
            _cinemachineVirtualCamera = FindObjectOfType<CinemachineVirtualCamera>();
            _cinemachineVirtualCamera.Follow = PlayerController.Instance.transform;
            PlayerController.Instance.ResetCamera();
        }
    }
}
