﻿using Player;
using UnityEngine;

namespace Weapons
{
    public class Bow : MonoBehaviour, IWeapon
    {
        [SerializeField] private Projectile arrow;
        [SerializeField] private Transform arrowSpawnPoint;
        [SerializeField] private WeaponInfo weaponInfo;

        private Animator _animator;
        private static readonly int Fire = Animator.StringToHash("Fire");

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public void Attack()
        {
            _animator.SetTrigger(Fire);
            Projectile arrowInstance = Instantiate(arrow, arrowSpawnPoint.position, ActiveWeapon.Instance.transform.rotation);
            arrowInstance.UpdateProjectileRange(weaponInfo.weaponRange);
        }

        public WeaponInfo GetWeaponInfo()
        {
            return weaponInfo;
        }
    }
}