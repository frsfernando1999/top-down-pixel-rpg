﻿namespace Weapons
{
    public interface IWeapon
    {
        public void Attack();
        public WeaponInfo GetWeaponInfo();

    }
}