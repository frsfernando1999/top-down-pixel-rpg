using System.Collections;
using Misc;
using UnityEngine;

namespace Weapons
{
    public class MagicLaser : MonoBehaviour
    {
        [SerializeField] private float laserGrowTime = 0.2f;
        
        private float _laserRange;
        private SpriteRenderer _spriteRenderer;
        private CapsuleCollider2D _collider;
        private Vector2 _colliderInitialSize;

        private bool _isGrowing = true;

        private void Awake()
        {
            _collider = GetComponent<CapsuleCollider2D>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.GetComponent<Indestructible>() && !col.isTrigger)
            {
                _isGrowing = false;
            }
        }

        public void SetLaserRange(float range)
        {
            _laserRange = range;
            StartCoroutine(IncreaseLaserLengthRoutine());
        }

        private IEnumerator IncreaseLaserLengthRoutine()
        {
            float timePassed = 0;
            while (_spriteRenderer.size.x < _laserRange && _isGrowing)
            {
                timePassed += Time.deltaTime;
                float linerT = timePassed / laserGrowTime;

                _spriteRenderer.size = new Vector2(Mathf.Lerp(1, _laserRange, linerT), 1f);
                
                Vector2 colliderSize = new Vector2(Mathf.Lerp(_colliderInitialSize.x, _laserRange, linerT), _collider.size.y);
                
                _collider.size = colliderSize;
                _collider.offset = new Vector2(colliderSize.x / 2, _collider.offset.y);
                yield return null;
            }

            StartCoroutine(GetComponent<SpriteFade>().SlowFadeRoutine());
        }
    }
}