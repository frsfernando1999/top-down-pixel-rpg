using Player;
using UnityEngine;

namespace Weapons
{
    public class Sword : MonoBehaviour, IWeapon
    {
        [SerializeField] private GameObject slashAnimationPrefab;
        [SerializeField] private WeaponInfo weaponInfo;

        private Transform _slashAnimationSpawnPoint;
        private Transform _weaponCollider;
        private Animator _animator;
        private GameObject _slashAnim;
        private Camera _camera;

        private static readonly int Swing = Animator.StringToHash("Swing");

        private void Awake()
        {
            _camera = Camera.main;
            _animator = GetComponent<Animator>();
        }

        private void Update()
        {
            MouseFollowWithOffset();
        }

        private void Start()
        {
            _weaponCollider = PlayerController.Instance.GetWeaponCollider();
            _slashAnimationSpawnPoint = PlayerController.Instance.GetSlashingSpawnPoint();
        }

        public void Attack()
        {
            _animator.SetTrigger(Swing);
            _weaponCollider.gameObject.SetActive(true);
            _slashAnim = Instantiate(slashAnimationPrefab, _slashAnimationSpawnPoint.position, Quaternion.identity);
            _slashAnim.transform.parent = transform.parent;
        }

        public WeaponInfo GetWeaponInfo()
        {
            return weaponInfo;
        }


        public void DoneAttackingAnim()
        {
            _weaponCollider.gameObject.SetActive(false);
        }

        public void SwingUpFlipAnim()
        {
            _slashAnim.gameObject.transform.rotation = Quaternion.Euler(-180, 0, 0);
            if (PlayerController.Instance.FacingLeft)
            {
                _slashAnim.GetComponent<SpriteRenderer>().flipX = true;
            }
        }

        public void SwingDownFlipAnim()
        {
            _slashAnim.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
            if (PlayerController.Instance.FacingLeft)
            {
                _slashAnim.GetComponent<SpriteRenderer>().flipX = true;
            }
        }

        private void MouseFollowWithOffset()
        {
            if (_camera == null)
            {
                _camera = Camera.main;
                return;
            }

            Vector3 playerScreenPoint = _camera.WorldToScreenPoint(PlayerController.Instance.transform.position);
            Vector3 mousePosition = Input.mousePosition;

            float angle = Mathf.Atan2(mousePosition.y, mousePosition.x) * Mathf.Rad2Deg;

            if (mousePosition.x < playerScreenPoint.x)
            {
                ActiveWeapon.Instance.transform.rotation = Quaternion.Euler(0, 180, angle);
                _weaponCollider.transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            else
            {
                ActiveWeapon.Instance.transform.rotation = Quaternion.Euler(0, 0, angle);
                _weaponCollider.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }
    }
}