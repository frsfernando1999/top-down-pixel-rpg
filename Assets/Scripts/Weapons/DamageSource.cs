using Enemies;
using Player;
using UnityEngine;

namespace Weapons
{
    public class DamageSource : MonoBehaviour
    {
        private int _damageAmount;

        private void Start()
        {
            MonoBehaviour currentActiveWeapon = ActiveWeapon.Instance.CurrentActiveWeapon;
            _damageAmount = ((IWeapon)currentActiveWeapon).GetWeaponInfo().weaponDamage;
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            EnemyHealth enemyHealth = col.gameObject.GetComponent<EnemyHealth>();
            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(_damageAmount);
            }
        }
    }
}
