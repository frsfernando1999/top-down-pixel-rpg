﻿using UnityEngine;

namespace Weapons
{
    [CreateAssetMenu(fileName = "New Weapon Info", menuName = "New Weapon Info", order = 0)]
    public class WeaponInfo : ScriptableObject
    {
        public GameObject weaponPrefab;
        public int weaponDamage;
        public float weaponCooldown;
        public float weaponRange;
        
    }
}