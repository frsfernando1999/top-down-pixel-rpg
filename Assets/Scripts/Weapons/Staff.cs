﻿using UnityEngine;

namespace Weapons
{
    public class Staff : MonoBehaviour, IWeapon
    {
        [SerializeField] private WeaponInfo weaponInfo;
        [SerializeField] private MagicLaser laserPrefab;
        [SerializeField] private Transform laserSpawnPoint;

        private Animator _animator;
        private static readonly int Fire = Animator.StringToHash("Fire");

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public void Attack()
        {
            _animator.SetTrigger(Fire);
        }

        public void SpawnStaffLaserAnimEvent()
        {
            MagicLaser newLaser = Instantiate(laserPrefab, laserSpawnPoint.position, laserSpawnPoint.rotation);
            newLaser.SetLaserRange(GetWeaponInfo().weaponRange);
        }
        public WeaponInfo GetWeaponInfo()
        {
            return weaponInfo;
        }

    }
}