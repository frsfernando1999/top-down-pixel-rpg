using Enemies;
using Misc;
using Player;
using UnityEngine;

namespace Weapons
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float moveSpeed = 25f;
        [SerializeField] private ParticleSystem destroyVFX;
        [SerializeField] private bool isEnemyProjectile;

        [SerializeField] private float projectileRange = 10f;
        private Vector3 _startPosition;

        private void Start()
        {
            _startPosition = transform.position;
        }

        private void Update()
        {
            MoveProjectile();
            DetectFireDistance();
        }

        public void UpdateProjectileRange(float range)
        {
            projectileRange = range;
        }
        public void UpdateMoveSpeed(float speed)
        {
            moveSpeed = speed;
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            EnemyHealth enemyHealth = col.gameObject.GetComponent<EnemyHealth>();
            PlayerHealth playerHealth = col.gameObject.GetComponent<PlayerHealth>();
            Indestructible indestructible = col.gameObject.GetComponent<Indestructible>();
            if (!col.isTrigger && (enemyHealth || indestructible || playerHealth))
            {
                var currentTransform = transform;
                if ((playerHealth && isEnemyProjectile) || (enemyHealth && !isEnemyProjectile))
                {
                    if (playerHealth) playerHealth.TakeDamage(1, transform);

                    Instantiate(destroyVFX, currentTransform.position, currentTransform.rotation);
                    Destroy(gameObject);
                }
                else if (!col.isTrigger && indestructible)
                {
                    Instantiate(destroyVFX, currentTransform.position, currentTransform.rotation);
                    Destroy(gameObject);
                }
            }
        }

        private void DetectFireDistance()
        {
            if (Vector3.Distance(transform.position, _startPosition) > projectileRange)
            {
                Destroy(gameObject);
                var currentTransform = transform;
                Instantiate(destroyVFX, currentTransform.position, currentTransform.rotation);
            }
        }

        private void MoveProjectile()
        {
            transform.Translate(Vector3.right * (moveSpeed * Time.deltaTime));
        }
    }
}