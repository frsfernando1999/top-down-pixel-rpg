using Misc;
using TMPro;
using UnityEngine;

namespace Player
{
    public class GoldManager : Singleton<GoldManager>
    {
        private TextMeshProUGUI _goldText;
        private int _currentGold;

        private const string GoldAmountText = "Gold Amount Text";
        private const int MaxGold = 99999;

        protected override void Awake()
        {
            base.Awake();
            _goldText = GameObject.Find(GoldAmountText).GetComponent<TextMeshProUGUI>();
        }

        private void Start()
        {
            UpdateGoldText();
        }

        public void GainGold(int goldToGain)
        {
            _currentGold = Mathf.Clamp(_currentGold + goldToGain, 0, MaxGold);
            UpdateGoldText();
        }

        private void UpdateGoldText()
        {
            if (_goldText == null)
            {
                _goldText = GameObject.Find(GoldAmountText).GetComponent<TextMeshProUGUI>();
            }
            else
            {
                _goldText.text = _currentGold.ToString().PadLeft(5, '0');
            }
        }

        public void LoseGold()
        {
            _currentGold /= 2;
            UpdateGoldText();
        }
    }
}