using System;
using System.Collections;
using Misc;
using UnityEngine;

namespace Player
{
    public class PlayerController : Singleton<PlayerController>
    {
        [SerializeField] private float moveSpeed = 8f;
        [SerializeField] private float walkSpeed = 3f;
        [SerializeField] private float dashSpeed = 3f;
        [SerializeField] private float dashTime = 0.1f;
        [SerializeField] private float dashCooldown = 0.25f;
        [SerializeField] private TrailRenderer trailRenderer;
        [SerializeField] private Transform weaponCollider;
        [SerializeField] private Transform slashingAnimationSpawnPoint;

        private PlayerControls _playerControls;
        private Vector2 _movement;
        private Camera _camera;

        private Rigidbody2D _rigidbody2D;
        private Animator _animator;
        private SpriteRenderer _spriteRenderer;
        private KnockBack _knockBack;
        private float _startingMoveSpeed;

        private bool _facingLeft;
        private bool _canDash = true;

        public bool FacingLeft => _facingLeft;

        private static readonly int MoveX = Animator.StringToHash("MoveX");
        private static readonly int MoveY = Animator.StringToHash("MoveY");

        protected override void Awake() 
        {
            base.Awake();
            SetDependencies();
        }

        private void Start()
        {
            _startingMoveSpeed = moveSpeed;
        }
        

        private void Walk()
        {
            _canDash = !_canDash;
            moveSpeed = Mathf.Approximately(moveSpeed, walkSpeed) ? _startingMoveSpeed : walkSpeed;
        }

        private void SetDependencies()
        {
            _camera = Camera.main;
            _rigidbody2D = GetComponent<Rigidbody2D>();
            _animator = GetComponent<Animator>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _knockBack = GetComponent<KnockBack>();
        }

        private void Update()
        {
            PlayerInput();
        }

        private void OnEnable()
        {
            if (_playerControls == null)
            {
                _playerControls = new PlayerControls();
            }
            _playerControls.Movement.Enable();
            _playerControls.Combat.Enable();
            
            _playerControls.Combat.Dash.performed += (_) => Dash();
            _playerControls.Movement.Walk.performed += (_) => Walk();
        }

        private void OnDisable()
        {
            if (_playerControls == null) return;
            _playerControls.Movement.Disable();
            _playerControls.Combat.Disable();
        }

        private void FixedUpdate()
        {
            Move();
            UpdatePlayerDirection();
        }

        public Transform GetWeaponCollider()
        {
            return weaponCollider;
        } 

        private void PlayerInput()
        {
            _movement = _playerControls.Movement.Move.ReadValue<Vector2>();
            _animator.SetFloat(MoveX, _movement.x);
            _animator.SetFloat(MoveY, _movement.y);
        }

        private void Move()
        {
            if (_knockBack.GettingKnockedBack) return;
            _rigidbody2D.MovePosition(_rigidbody2D.position + _movement * (moveSpeed * Time.fixedDeltaTime));
        }

        private void UpdatePlayerDirection()
        {
            if (_camera == null) return;
            
            Vector3 mousePosition = _camera.WorldToScreenPoint(_rigidbody2D.position) - Input.mousePosition;
            _facingLeft = mousePosition.x > 0;
            _spriteRenderer.flipX = _facingLeft;
        }

        private void Dash()
        {
            if (!_canDash || PlayerStamina.Instance.CurrentStamina == 0) return;
            _canDash = false;
            moveSpeed *= dashSpeed;
            trailRenderer.emitting = true;
            PlayerStamina.Instance.UseStamina(1);
            StartCoroutine(EndDashRoutine());
        }

        private IEnumerator EndDashRoutine()
        {
            yield return new WaitForSeconds(dashTime);
            trailRenderer.emitting = false;
            moveSpeed = _startingMoveSpeed;
            yield return new WaitForSeconds(dashCooldown);
            _canDash = true;
        }

        public void ResetCamera()
        {
            _camera = Camera.main;
        }

        public Transform GetSlashingSpawnPoint()
        {
            return slashingAnimationSpawnPoint;
        }

        public Camera GetCamera()
        {
            return _camera;
        }
    }
}