using System.Collections;
using Misc;
using UnityEngine;
using Weapons;

namespace Player
{
    public class ActiveWeapon : Singleton<ActiveWeapon>
    {
        private MonoBehaviour _currentActiveWeapon;
        private PlayerControls _playerControls;

        private bool _attackButtonDown;
        private bool _isAttacking;

        public MonoBehaviour CurrentActiveWeapon => _currentActiveWeapon;

        private void OnEnable()
        {
            if (_playerControls == null)
            {
                _playerControls = new PlayerControls();
            }

            _playerControls.Combat.Enable();
        }

        private void OnDisable()
        {
            if (_playerControls == null) return;
            _playerControls.Combat.Disable();
        }

        private void Start()
        {
            _playerControls.Combat.Attack.started += _ => StartAttacking();
            _playerControls.Combat.Attack.canceled += _ => StopAttacking();
        }

        private void Update()
        {
            Attack();
        }

        public void NewWeapon(MonoBehaviour newWeapon)
        {
            if (_currentActiveWeapon != null)
            {
                Destroy(_currentActiveWeapon.gameObject);
            }

            _currentActiveWeapon = newWeapon;
        }

        private void Attack()
        {
            if (_currentActiveWeapon == null) return;

            if (!_isAttacking && _attackButtonDown)
            {
                _isAttacking = true;
                if (_currentActiveWeapon is not IWeapon weapon) return;
                weapon.Attack();
                StartCoroutine(AttackCooldownRoutine(weapon.GetWeaponInfo().weaponCooldown));
            }
        }

        private IEnumerator AttackCooldownRoutine(float weaponCooldown)
        {
            yield return new WaitForSeconds(weaponCooldown);
            _isAttacking = false;
        }

        private void StartAttacking() => _attackButtonDown = true;

        private void StopAttacking() => _attackButtonDown = false;
    }
}