using System;
using System.Collections;
using Misc;
using UnityEngine;

namespace Player
{
    public class PlayerStamina : Singleton<PlayerStamina>
    {
        [SerializeField] private int maxStamina = 5;
        [SerializeField] [Min(0.1f)] private float restoreStaminaTime = 3f;

        public event Action UpdatedStamina;

        private int _currentStamina;

        public int MaxStamina => maxStamina;
        public int CurrentStamina => _currentStamina;

        protected override void Awake()
        {
            base.Awake();
            _currentStamina = maxStamina;
        }

        private void Start()
        {
            StartCoroutine(StaminaRegenRoutine());
        }

        private IEnumerator StaminaRegenRoutine()
        {
            while (true)
            {
                if (_currentStamina < MaxStamina) RestoreStamina(1);
                yield return new WaitForSeconds(restoreStaminaTime);
            }
        }

        public void RestoreStamina(int recoverAmount)
        {
            _currentStamina = Mathf.Clamp(_currentStamina + recoverAmount, 0, maxStamina);
            UpdatedStamina?.Invoke();
        }

        public void UseStamina(int useAmount)
        {
            _currentStamina = Mathf.Clamp(_currentStamina - useAmount, 0, maxStamina);
            UpdatedStamina?.Invoke();
        }
    }
}