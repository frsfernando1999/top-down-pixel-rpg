using UnityEngine;
using Weapons;

namespace Player
{
    public class InventorySlot : MonoBehaviour
    {
        [SerializeField] private WeaponInfo weaponInfo;

        public WeaponInfo GetWeaponInfo()
        {
            return weaponInfo;
        }
    }
}
