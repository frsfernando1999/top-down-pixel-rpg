using System;
using System.Collections;
using Enemies;
using Misc;
using Transition;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Player
{
    public class PlayerHealth : Singleton<PlayerHealth>
    {
        [SerializeField] private int maxHealth = 5;
        [SerializeField] private float iFramesTime = 0.2f;
        [SerializeField] private float deathReloadTime = 2f;
        [SerializeField] private float knockBackThrustAmount = 10f;
        
        public event Action UpdatedHealth; 

        private KnockBack _knockBack;
        private Flash _flash;
        private bool _isInvincible;
        
        private int _currentHealth;
        
        private static readonly int Death = Animator.StringToHash("Death");
        private static readonly int Revived = Animator.StringToHash("Revived");
        private const string SceneName = "Scene_1";


        public int MaxHealth => maxHealth;
        public int CurrentHealth => _currentHealth;

        protected override void Awake()
        {
            base.Awake();
            _knockBack = GetComponent<KnockBack>();
            _flash = GetComponent<Flash>();
            _currentHealth = maxHealth;

        }
        
        private void OnCollisionStay2D(Collision2D collision)
        {
            EnemyAI enemy = collision.gameObject.GetComponent<EnemyAI>();

            if (enemy)
            {
                TakeDamage(1, collision.transform);
            }
        }

        public void Heal(int healAmount)
        {
            _currentHealth = Mathf.Clamp(_currentHealth + healAmount, 0, maxHealth);
            UpdatedHealth?.Invoke();
        }

        public void TakeDamage(int damageAmount, Transform hitTransform)
        {
            if (_isInvincible) return;  
            ScreenShakeManager.Instance.ShakeScreen();
            _isInvincible = true;
            _knockBack.GetKnockedBack(hitTransform, knockBackThrustAmount);
            StartCoroutine(_flash.FlashRoutine());
            _currentHealth = Mathf.Clamp(_currentHealth - damageAmount, 0, maxHealth);
            UpdatedHealth?.Invoke();
            if (_currentHealth == 0)
            {
                Die();
            }
            else
            {
                StartCoroutine(TurnOffIFrames());
            }
        }

        private void Die()
        {
            GetComponent<Animator>().SetTrigger(Death);
            GoldManager.Instance.LoseGold();
            PlayerController.Instance.enabled = false;
            StartCoroutine(DeathLoadSceneRoutine());
        }

        private IEnumerator DeathLoadSceneRoutine()
        {
            UIFade.Instance.FadeToBlack();
            yield return new WaitForSeconds(deathReloadTime);
            SceneManager.LoadScene(SceneName);
            UIFade.Instance.FadeClear();
            Heal(maxHealth);
            PlayerStamina.Instance.RestoreStamina(PlayerStamina.Instance.MaxStamina);
            GetComponent<Animator>().SetTrigger(Revived);
            PlayerController.Instance.enabled = true;
            TransitionManager.Instance.SetTransitionName(PortalIdentifier.RespawnA);
            _isInvincible = false;
        }


        private IEnumerator TurnOffIFrames()
        {
            yield return new WaitForSeconds(iFramesTime);
            _isInvincible = false;
        }
    }
}