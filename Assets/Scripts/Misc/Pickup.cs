using System;
using System.Collections;
using Player;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Misc
{
    public class Pickup : MonoBehaviour
    {
        private enum PickupType
        {
            Health,
            Gold,
            Stamina
        }

        [SerializeField] private float pickupDistance = 5f;
        [SerializeField] private float pickupAcceleration = 2f;
        [SerializeField] private float moveSpeed = 5f;
        [SerializeField] private AnimationCurve animCurve;
        [SerializeField] private float heightY = 1.5f;
        [SerializeField] private float popDuration = 0.75f;
        [SerializeField] private float maxPopRange = 1.5f;
        [SerializeField] private PickupType pickupType;

        private Vector3 _moveDir;

        private Rigidbody2D _rigidbody2D;
        private float _startSpeed;

        private void Awake()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
        }

        private void Start()
        {
            _startSpeed = moveSpeed;
            StartCoroutine(AnimCurveSpawnRoutine());
        }

        private void Update()
        {
            Vector3 playerPos = PlayerController.Instance.transform.position;
            if (Vector3.Distance(transform.position, playerPos) < pickupDistance)
            {
                _moveDir = (playerPos - transform.position).normalized;
                moveSpeed += pickupAcceleration;
            }
            else
            {
                _moveDir = Vector3.zero;
                moveSpeed = _startSpeed;
            }
        }

        private void FixedUpdate()
        {
            _rigidbody2D.velocity = _moveDir * (moveSpeed * Time.deltaTime);
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.GetComponent<PlayerController>())
            {
                DetectPickupType();
                Destroy(gameObject);
            }
        }

        private IEnumerator AnimCurveSpawnRoutine()
        {
            float timePassed = 0;
            Vector2 startPos = transform.position;
            
            float randomX = startPos.x + Random.Range(-maxPopRange, maxPopRange);
            float randomY = startPos.y + Random.Range(-maxPopRange, maxPopRange);
            
            Vector2 endPos = new Vector2(randomX, randomY);

            while (timePassed < popDuration)
            {
                timePassed += Time.deltaTime;
                float linearT = timePassed / popDuration;
                float heightT = animCurve.Evaluate(linearT);
                float height = Mathf.Lerp(0f, heightY, heightT);
                transform.position = Vector2.Lerp(startPos, endPos, linearT) + new Vector2(0f, height);
                yield return null;
            }
        }

        private void DetectPickupType()
        {
            switch (pickupType)
            {
                case PickupType.Health:
                    PlayerHealth.Instance.Heal(1);
                    break;
                case PickupType.Gold:
                    GoldManager.Instance.GainGold(1);
                    break;
                case PickupType.Stamina:
                    PlayerStamina.Instance.RestoreStamina(1);
                    break;
            }
        }
    }
}