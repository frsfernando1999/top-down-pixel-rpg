using UnityEngine;

namespace Misc
{
    public class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        public static T Instance { get; private set; }

        protected virtual void Awake()
        {
            if (Instance != null && gameObject != null && Instance != this)
            {
                    Destroy(gameObject);
            }
            else
            {
                Instance = (T)this;
            }

            if (!gameObject.transform.parent)
            {
                DontDestroyOnLoad(gameObject);
            }
        }
    }
}