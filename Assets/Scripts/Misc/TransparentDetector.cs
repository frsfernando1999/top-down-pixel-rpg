using System.Collections;
using Player;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Misc
{
    public class TransparentDetector : MonoBehaviour
    {
        [Range(0, 1)] [SerializeField] private float transparencyAmount = 0.8f;
        [SerializeField] private float transparencyFadeTime = 0.4f;

        private SpriteRenderer _spriteRenderer;
        private Tilemap _tileMap;

        private void Awake()
        {
            _tileMap = GetComponent<Tilemap>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.GetComponent<PlayerController>())
            {
                if (_spriteRenderer)
                {
                    StartCoroutine(FadeRoutine(_spriteRenderer, transparencyFadeTime, _spriteRenderer.color.a,
                        transparencyAmount));
                }
                else if (_tileMap)
                {
                    StartCoroutine(FadeRoutine(_tileMap, transparencyFadeTime, _tileMap.color.a,
                        transparencyAmount));
                }
            }
        }

        private void OnTriggerExit2D(Collider2D col)
        {
            if (col.gameObject.GetComponent<PlayerController>())
            {
                if (_spriteRenderer)
                {
                    StartCoroutine(FadeRoutine(_spriteRenderer, transparencyFadeTime, _spriteRenderer.color.a,
                        1f));
                }
                else if (_tileMap)
                {
                    StartCoroutine(FadeRoutine(_tileMap, transparencyFadeTime, _tileMap.color.a,
                        1f));
                }
            }
        }

        private IEnumerator FadeRoutine(SpriteRenderer spriteRenderer, float fadeTime, float startValue,
            float targetTransparency)
        {
            float elapsedTime = 0f;
            while (elapsedTime < fadeTime)
            {
                elapsedTime += Time.deltaTime;
                float newAlpha = Mathf.Lerp(startValue, targetTransparency, elapsedTime / fadeTime);
                var rendererColor = spriteRenderer.color;
                spriteRenderer.color = new Color(rendererColor.r, rendererColor.g, rendererColor.b, newAlpha);
                yield return null;
            }
        }

        private IEnumerator FadeRoutine(Tilemap tilemap, float fadeTime, float startValue,
            float targetTransparency)
        {
            float elapsedTime = 0f;
            while (elapsedTime < fadeTime)
            {
                elapsedTime += Time.deltaTime;
                float newAlpha = Mathf.Lerp(startValue, targetTransparency, elapsedTime / fadeTime);
                var rendererColor = tilemap.color;
                tilemap.color = new Color(rendererColor.r, rendererColor.g, rendererColor.b, newAlpha);
                yield return null;
            }
        }
    }
}