using UnityEngine;

namespace Misc
{
    public class PickupSpawner : MonoBehaviour
    {
        [SerializeField] private Pickup goldCoin;
        [SerializeField] private Pickup health;
        [SerializeField] private Pickup staminaGlobe;

        public void DropItems()
        {
            int randomNum = Random.Range(1, 5);
            switch (randomNum)
            {
                case 1:
                    Instantiate(staminaGlobe, transform.position, Quaternion.identity);
                    break;
                case 2:
                    Instantiate(health, transform.position, Quaternion.identity);
                    break;
                case 3:
                    int randomAmountOfGold = Random.Range(1, 4);
                    for (int i = 0; i < randomAmountOfGold; i++)
                    {
                        Instantiate(goldCoin, transform.position, Quaternion.identity);
                    }
                    break;
            }
        }
    }
}
