using System;
using UnityEngine;

namespace Misc
{
    public class CursorManager : MonoBehaviour
    {
        private Camera _camera;
        void Start()
        {
            Cursor.visible = false;
            if (Application.isPlaying)
            {
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Confined;
            }
        }

        void LateUpdate()
        {
            Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = cursorPos;

            // if (!Application.isPlaying) { return; }
            //
            // Cursor.visible = false;
        }
    }
}
