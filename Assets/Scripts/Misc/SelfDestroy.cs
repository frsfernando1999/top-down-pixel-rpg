using UnityEngine;

namespace Misc
{
    public class SelfDestroy : MonoBehaviour
    {
        private ParticleSystem _ps;

        private void Awake()
        {
            _ps = GetComponent<ParticleSystem>();
        }

        private void Update()
        {
            if (_ps && !_ps.IsAlive()) 
            {
                DestroySelfAnimEvent();  
            }
        }

        public void DestroySelfAnimEvent()
        {
            Destroy(gameObject);
        }
    }
}
