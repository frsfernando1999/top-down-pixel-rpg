    using System.Collections;
using UnityEngine;

namespace Misc
{
    public class SpriteFade : MonoBehaviour
    {
        [SerializeField] private float fadeTime = .25f;
        
        private SpriteRenderer _spriteRenderer;

        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public IEnumerator SlowFadeRoutine()
        {
            float elapsedTime = 0f;
            float startValue = _spriteRenderer.color.a;
            while (elapsedTime < fadeTime)
            {
                elapsedTime += Time.deltaTime;
                float newAlpha = Mathf.Lerp(startValue, 0f, elapsedTime / fadeTime);
                var rendererColor = _spriteRenderer.color;
                _spriteRenderer.color = new Color(rendererColor.r, rendererColor.g, rendererColor.b, newAlpha);
                yield return null;
            }
            
            Destroy(gameObject);
        }
    }
}
