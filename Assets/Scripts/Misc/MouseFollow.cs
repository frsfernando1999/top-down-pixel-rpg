using Player;
using UnityEngine;

namespace Misc
{
    public class MouseFollow : MonoBehaviour
    {
        private void Update()
        {
            FaceMouse();
        }

        private void FaceMouse()
        {
            Vector3 mousePos = Input.mousePosition;
            Camera playerCam = PlayerController.Instance.GetCamera();
            if (playerCam == null) return;
            mousePos = playerCam.ScreenToWorldPoint(mousePos);
            Vector2 direction = transform.position - mousePos;

            transform.right = -direction;
        }
    }
}
