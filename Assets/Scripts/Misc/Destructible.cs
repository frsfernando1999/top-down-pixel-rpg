using UnityEngine;
using Weapons;

namespace Misc
{
    public class Destructible : MonoBehaviour
    {
        [SerializeField] private ParticleSystem destroyVFX;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.GetComponent<DamageSource>() || other.gameObject.GetComponent<Projectile>())
            {
                PickupSpawner spawner = GetComponent<PickupSpawner>();
                if (spawner) spawner.DropItems();
                Instantiate(destroyVFX, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}
