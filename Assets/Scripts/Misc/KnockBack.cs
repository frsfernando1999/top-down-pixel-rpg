using System.Collections;
using UnityEngine;

namespace Misc
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class KnockBack : MonoBehaviour
    {
        [SerializeField] private float knockBackTime = 0.1f;
        public bool GettingKnockedBack { get; private set; }
        private Rigidbody2D _rigidbody2D;

        private void Awake()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
        }

        private IEnumerator KnockRoutine()
        {
            yield return new WaitForSeconds(knockBackTime);
            _rigidbody2D.velocity = Vector2.zero;
            GettingKnockedBack = false;
        }

        public void GetKnockedBack(Transform damageSource, float knockBackThrust)
        {
            GettingKnockedBack = true;
            Vector2 difference = (transform.position - damageSource.position).normalized * knockBackThrust * _rigidbody2D.mass;
            _rigidbody2D.AddForce(difference, ForceMode2D.Impulse);
            StartCoroutine(KnockRoutine());
        }
    }
}