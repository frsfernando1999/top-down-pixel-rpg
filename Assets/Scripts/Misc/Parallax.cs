using UnityEngine;

namespace Misc
{
    public class Parallax : MonoBehaviour
    {
        [SerializeField] private float parallaxOffset = -0.15f;
        private Camera _camera;
        private Vector2 _startPos;
        private Vector2 Travel => (Vector2)_camera.transform.position - _startPos;
    
        private void Awake()
        {
            _camera = Camera.main;
        }

        private void Start()
        {
            _startPos = transform.position;
        }

        private void FixedUpdate()
        {
            transform.position = _startPos + Travel * parallaxOffset;  
        }
    }
}
