using Player;
using UnityEngine;
using UnityEngine.InputSystem;
using Weapons;

namespace UI
{
    public class ActiveInventory : MonoBehaviour
    {
        private int _activeSlotIndex;
        private PlayerControls _playerControls;

        private void OnEnable()
        {
            _playerControls ??= new PlayerControls();
            _playerControls.Inventory.Enable();
        }

        private void OnDisable()
        {
            _playerControls?.Inventory.Disable();
        }

        private void Start()
        {
            _playerControls.Inventory.KeyBindings.performed += ToggleActive;
            ToggleActiveSlot(0);
        }

        private void ToggleActive(InputAction.CallbackContext obj)
        {
            ToggleActiveSlot((int)obj.ReadValue<float>() - 1);
        }

        private void ToggleActiveSlot(int index)
        {
            if (!transform.GetChild(index).GetComponentInChildren<InventorySlot>().GetWeaponInfo()) return;
            
            _activeSlotIndex = index;
            
            foreach (Transform inventorySlot in transform)
            {
                inventorySlot.GetChild(0).gameObject.SetActive(false);
            }
            
            var activeSlot = transform.GetChild(_activeSlotIndex);
            activeSlot.GetChild(0).gameObject.SetActive(true);
            ChangeActiveWeapon(activeSlot);
        }

        private void ChangeActiveWeapon(Transform activeSlot)
        {
            var child = activeSlot.GetComponentInChildren<InventorySlot>();
            WeaponInfo weaponToSpawn = child.GetWeaponInfo();
            GameObject newWeapon = Instantiate(weaponToSpawn.weaponPrefab, ActiveWeapon.Instance.transform);
            ActiveWeapon.Instance.NewWeapon(newWeapon.GetComponent<MonoBehaviour>());
        }
    }
}