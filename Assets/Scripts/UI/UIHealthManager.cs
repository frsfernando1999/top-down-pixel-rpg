using Player;
using UnityEngine;

namespace UI
{
    public class UIHealthManager : MonoBehaviour
    {
        [SerializeField] private GameObject healthIconPrefab;

        private PlayerHealth _playerHealth;

        private void Awake()
        {
            _playerHealth = GameObject.FindWithTag("Player").GetComponent<PlayerHealth>();
        }

        private void OnEnable()
        {
            _playerHealth.UpdatedHealth += UpdateHealthIcons;
        }

        private void OnDisable()
        {
            _playerHealth.UpdatedHealth -= UpdateHealthIcons;
        }

        private void Start()
        {
            UpdateHealthIcons();
        }

        private void UpdateHealthIcons()
        {
            DestroyIcons();
            var healthIcons = CreateHealthIcons();
            SetLostHealth(healthIcons);
        }

        private void DestroyIcons()
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }
        }

        private void SetLostHealth(GameObject[] healthIcons)
        {
            for (int i = _playerHealth.CurrentHealth; i < _playerHealth.MaxHealth; i++)
            {
                healthIcons[i].transform.GetChild(0).gameObject.SetActive(false);
                healthIcons[i].transform.GetChild(1).gameObject.SetActive(true);
            }
        }

        private GameObject[] CreateHealthIcons()
        {
            GameObject[] healthIcons = new GameObject[PlayerHealth.Instance.MaxHealth];
            for (int i = 0; i < PlayerHealth.Instance.MaxHealth; i++)
            {
                healthIcons[i] = Instantiate(healthIconPrefab, transform);
            }

            return healthIcons;
        }
    }
}
