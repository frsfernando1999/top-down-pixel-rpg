using System.Collections;
using Misc;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIFade : Singleton<UIFade>
    {
        [SerializeField] private Image fadeScreen;
        [SerializeField] private float fadeTime = 1f;

        public float FadeTime => fadeTime;

        private IEnumerator _fadeRoutine;

        public void FadeToBlack()
        {
            if (_fadeRoutine != null)
            {
                StopCoroutine(_fadeRoutine);
            }
            _fadeRoutine = FadeRoutine(1f);
            StartCoroutine(_fadeRoutine);
        }

        public void FadeClear()
        {
            if (_fadeRoutine != null)
            {
                StopCoroutine(_fadeRoutine);
            }
            _fadeRoutine = FadeRoutine(0f);
            StartCoroutine(_fadeRoutine);

        }

        private IEnumerator FadeRoutine(float targetAlpha)
        {
            while (!Mathf.Approximately(fadeScreen.color.a, targetAlpha))
            {
                float alpha = Mathf.MoveTowards(fadeScreen.color.a, targetAlpha, fadeTime * Time.deltaTime);
                fadeScreen.color = new Color(fadeScreen.color.r, fadeScreen.color.g, fadeScreen.color.b, alpha);
                yield return null;
            }
        }

    }
}
