using Player;
using UnityEngine;

namespace UI
{
    public class StaminaUIManager : MonoBehaviour
    {
        [SerializeField] private GameObject staminaIconPrefab;

        private PlayerStamina _playerStamina;

        private void Awake()
        {
            _playerStamina = GameObject.FindWithTag("Player").GetComponent<PlayerStamina>();
        }

        private void OnEnable()
        {
            _playerStamina.UpdatedStamina += UpdateStaminaIcons;
        }

        private void OnDisable()
        {
            _playerStamina.UpdatedStamina -= UpdateStaminaIcons;
        }

        private void Start()
        {
            UpdateStaminaIcons();
        }

        private void UpdateStaminaIcons()
        {
            DestroyIcons();
            var staminaIcons = CreateStaminaIcons();
            SetLostStamina(staminaIcons);
        }

        private void DestroyIcons()
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }
        }

        private void SetLostStamina(GameObject[] staminaIcons)
        {
            for (int i = _playerStamina.CurrentStamina; i < _playerStamina.MaxStamina; i++)
            {
                staminaIcons[i].transform.GetChild(0).gameObject.SetActive(false);
                staminaIcons[i].transform.GetChild(1).gameObject.SetActive(true);
            }
        }

        private GameObject[] CreateStaminaIcons()
        {
            GameObject[] staminaIcons = new GameObject[PlayerStamina.Instance.MaxStamina];
            for (int i = 0; i < _playerStamina.MaxStamina; i++)
            {
                staminaIcons[i] = Instantiate(staminaIconPrefab, transform);
            }

            return staminaIcons;
        }
    }
}
